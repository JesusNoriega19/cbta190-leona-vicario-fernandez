import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'inicio', loadChildren: () => import('./components/home/home.module').then((m) => m.HomeModule) },
  { path: 'carreras', loadChildren: () => import('./components/careers/careers.module').then((m) => m.CareersModule) },
  { path: 'contacto', loadChildren: () => import('./components/contact/contact.module').then((m) => m.ContactModule) },
  { path: 'acerca-de', loadChildren: () => import('./components/about/about.module').then((m) => m.AboutModule) },
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
