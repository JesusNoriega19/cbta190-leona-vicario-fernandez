import { Component } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  scrollTo(target: string) {
    const element = document.getElementById(target);
    const headerHeight = 80;
    if (element) {
      const offset = element.offsetTop - headerHeight;
      window.scrollTo({
        top: offset,
        behavior: 'smooth',
      });
    }
  }
}


// constructor(private router: Router) {}

// onMenuLinkClick(target: string) {
//   this.router.navigate([target]);
// }

// isSticky = false;

// scrollTo(target: string) {
//   const element = document.getElementById(target);
//   const headerHeight = 80; /* ajusta esta variable con la altura de tu encabezado */;

//   if (element) {
//     const offset = element.offsetTop - headerHeight;
//     window.scrollTo({
//       top: offset,
//       behavior: 'smooth',
//     });
//   }
//   }