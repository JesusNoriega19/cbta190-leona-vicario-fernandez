import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadHome: boolean = false;
  loadCareers: boolean = false;
  loadContact: boolean = false;
  loadAbout: boolean = false;

  ngOnInit() {
    this.checkIfLoadComponent(); // Verifica inicialmente si debe cargar los componentes.
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: Event): void {
    this.checkIfLoadComponent();
  }
  
  checkIfLoadComponent() {
    const scrollPosition = window.scrollY || document.documentElement.scrollTop;
    const windowHeight = window.innerHeight;
  
    if (scrollPosition === 0 && !this.loadHome) {
      this.loadHome = true;
    }
    if (scrollPosition > windowHeight && !this.loadCareers) {
      this.loadCareers = true;
    }
    if (scrollPosition > 2 * windowHeight && !this.loadContact) {
      this.loadContact = true;
    }
    if (scrollPosition > 3 * windowHeight && !this.loadAbout) {
      this.loadAbout = true;
    }
  }
  
}
